package model;


import javax.persistence.*;

@Entity
public class CarPart {

    @Id
    @GeneratedValue
    @Column(name="carPart_id")
    private Long id;

    private String carPart_name;

    @ManyToOne
    private Car car_d;

    public CarPart() {
    }

    public CarPart(String carPart_name, Car car_d) {
        this.carPart_name = carPart_name;
        this.car_d = car_d;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCarPart_name() {
        return carPart_name;
    }

    public void setCarPart_name(String carPart_name) {
        this.carPart_name = carPart_name;
    }

    public Car getCar_d() {
        return car_d;
    }

    public void setCar_d(Car car_d) {
        this.car_d = car_d;
    }
}
