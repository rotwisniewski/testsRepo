package model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Car {

    @Id
    @GeneratedValue
    @Column(name="car_id")
    private Long id;

    private String car_name;

    @OneToMany(mappedBy = "car_d")
    private List<CarPart> carParts;


    public Car() {
    }

    public Car(String car_name, List<CarPart> carParts) {
        this.car_name = car_name;
        this.carParts = carParts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCar_name() {
        return car_name;
    }

    public void setCar_name(String car_name) {
        this.car_name = car_name;
    }

    public List<CarPart> getCarParts() {
        return carParts;
    }

    public void setCarParts(List<CarPart> carParts) {
        this.carParts = carParts;
    }
}
