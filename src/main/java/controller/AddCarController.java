package controller;


import model.Car;
import model.CarPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import repository.CarRepository;

@Controller
@RequestMapping("/addTrip")
public class AddCarController {

    private CarRepository carRepository;

    @Autowired
    public AddCarController(CarRepository carRepository){
        this.carRepository = carRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String setupForm(Model model){
        model.addAttribute("car", new Car());

        return "addCarForm";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String processForm(Car car){
        carRepository.save(car);

        return "home";
    }
}
